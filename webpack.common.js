const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'put title here',
            inject: false,
            template: require('html-webpack-template'),
            bodyHtmlSnippet: "<div id='app'></div>"
        })
    ],
    output: {
        filename: '[name]-[hash].js',
        path: path.resolve(__dirname, 'dist')
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
        ]
    },
    // see https://webpack.js.org/guides/asset-management for loading images, fonts or data

    resolve: {
        extensions: ["*", ".webpack.js", ".web.js", ".js", ".json", ".jsx"],
    }
};