# Basis-Projekt für React Anwendungen
Dies ist mein Template für React-Anwendungen. Es besteht aus den folgenden Bausteinen:
+ React,
+ Webpack,
+ Babel

## Benutzung
1. npm-Module installieren:

    ```npm install```

2. Während Entwicklung:

    ```npm run start```

    startet den Entwicklungs-Server
3. Für Production:

    ```npm run build```

    erzeugt die Dateien für den Web-Server und legt sie in den Ordner dist

## Autor
Benjamin Becker

## Lizenz
Dieses Repository darf frei verwendet werden.