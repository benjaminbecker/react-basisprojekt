import React from 'react';
import ReactDOM from 'react-dom';
import Content from './Content';

const App = () => {
    return (
        <div>
            <h1>Guten Tag</h1>
            <Content />
        </div>
    );
};

ReactDOM.render(
    <App />,
    document.getElementById('app')
  );
